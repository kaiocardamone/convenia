<?php
foreach($user->categories as $row) {
    $categoryArr[$row->id] = $row->name;
}

?>
<?php echo validation_errors('<div class="alert alert-danger" role="alert">','</div>');

?>
    <h1>Edit User</h1>
    <div class="col-lg-4">
        <form action="http://convenia.dev/UserController/update/<? echo $user->id ?>" method="post" accept-charset="utf-8">

            <div class="form-group">
                <label for="category_id">Category</label>
                <?php echo form_dropdown('category_id', $categoryArr, $user->category_id, 'class="form-control" ');?>
            </div>

            <div class="form-group">
                <label for="name">Name</label>
                <?php echo form_input(array('name' => 'name', 'class' => 'form-control', 'value' => $user->name), 'autofocus'); ?>
            </div>

            <div class="form-group">
                <label for="email">Email address</label>
                <?php echo form_input(array('name' => 'email', 'class' => 'form-control', 'type' => 'email', 'disabled' => 'disabled', 'value' => $user->email)); ?>
            </div>

            <div class="form-group">
                <label for="date_birthday">Date Birthday</label>
                <?php echo form_input(array('name' => 'date_birthday', 'class' => 'datepicker form-control', 'id' => 'date_birthday', 'value' => $user->date_birthday)); ?>
            </div>

            <div class="form-group">
                <button type="button" onclick="showPasswordUpdate();" class="btn btn-success">Change Password</button>
            </div>

            <div class="hiddenPassword hide">
                <div class="form-group">
                    <label for="password">Password</label>
                    <?php echo form_input(array('name' => 'password', 'id' => 'password', 'class' => 'form-control', 'type' => 'password')); ?>
                </div>

                <div class="form-group">
                    <label for="password_confirm">Password Confirm</label>
                    <?php echo form_input(array('name' => 'password_confirm', 'id' => 'confirmPassword', 'class' => 'form-control', 'type' => 'password')); ?>
                </div>



                <div class="form-group">
                    <button type="button" onclick="changePassword(<? echo $user->id ?>);" class="btn btn-primary">Apply</button>
                </div>

            </div>

            <p id="response"></p>

            <div class="form-group">
                <?php echo form_checkbox(array('name' => 'active', 'value' => 1, 'checked' => $user->active != 0 ? true : false)); ?>
                <label>Active</label>
            </div>
            <button type="submit" class="btn btn-success">Send</button>
        </form>
    </div>

