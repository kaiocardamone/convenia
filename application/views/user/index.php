<?php

if($this->session->flashdata('insertOk')){
    echo '<div class="alert alert-success" role="alert">'.$this->session->flashdata('insertOk').'</div>';
}
if($this->session->flashdata('updateOk')){
    echo '<div class="alert alert-success" role="alert">'.$this->session->flashdata('updateOk').'</div>';
}
if($this->session->flashdata('deleteOk')){
    echo '<div class="alert alert-success" role="alert">'.$this->session->flashdata('deleteOk').'</div>';
}
if($this->session->flashdata('deleteError')){
    echo '<div class="alert alert-danger" role="alert">'.$this->session->flashdata('deleteError').'</div>';
}
if($this->session->flashdata('loginSuccess')){
    echo '<div class="alert alert-success" role="alert">'.$this->session->flashdata('loginSuccess').'</div>';
}
?>

<h1>List of Users</h1>

<div class="panel panel-default">
    <!-- Default panel contents -->
    <div class="panel-heading">Users</div>
    <table class="table">
        <thead>
        <tr>
            <th>#</th>
            <th>Name</th>
            <th>Email</th>
            <th>Date Birthday</th>
            <th>Created</th>
            <th>Updated</th>
            <th>Active</th>
            <th>Category</th>
            <th>Actions</th>
        </tr>
        </thead>
        <tbody>
            <?php foreach($users as $user) { ?>
                <tr>
                    <th scope="row"><?php echo $user->user_id; ?></th>
                    <td><?php echo $user->user_name; ?></td>
                    <td><?php echo $user->email; ?></td>
                    <td><?php echo dateChangeHelper::enDateChangeToBrDate($user->date_birthday); ?></td>
                    <td><?php echo dateChangeHelper::enDateChangeToBrDate($user->created); ?></td>
                    <td><?php echo !empty($user->updated) ? dateChangeHelper::enDateChangeToBrDate($user->updated) : '-'; ?></td>
                    <td><?php echo $user->active == true ? 'Yes' : 'No'; ?></td>
                    <td><?php echo $user->category_name; ?></td>
                    <td>
                        <a href="/UserController/update/<?php echo $user->user_id; ?>"><span class="glyphicon glyphicon-edit"></span></a>
                        <a href="/UserController/delete/<?php echo $user->user_id; ?>"><span class="glyphicon glyphicon-remove"></span></a>
<!--                        <a href="/admin/employees/view/1"></a>-->
                    </td>
                </tr>
            <?php } ?>
        </tbody>
    </table>
</div>
<?php echo $pagination; ?>
<p><? echo "Number of records returned this page: " . sizeof( $users ); ?></p>























        <?


//            $this->table->set_heading('Name', 'Email', 'Birthday', 'Created', 'Updated', 'Active');
//            foreach($user as $line) {
//                $delete = '<a href="javascript:void(0)" id="delete_'.$line->id.'" onclick="deleteUser('.$line->id.')">Delete</a>';
//                $this->table->add_row(
//                    $line->name,
//                    $line->email,
//                    $line->date_birthday,
//                    $line->created,
//                    $line->updated,
//                    $line->active,
//                    anchor("UserController/update/$line->id", "Edit"),
//                    $delete
//                );
//            }
//            echo $this->table->generate();
        ?>
</div>