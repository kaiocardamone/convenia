<?php
foreach($category as $row) {
    $categoryArr[$row->id] = $row->name;
}

?>

<?php echo validation_errors('<div class="alert alert-danger" role="alert">','</div>');
?>

<h1>Adicionar Usuário</h1>
<div class="col-lg-4">
    <form action="http://convenia.dev/UserController/create" method="post" accept-charset="utf-8">

        <div class="form-group">
        <label for="category_id">Category</label>
            <?php echo form_dropdown('category_id', $categoryArr, '','class="form-control" ');?>
        </div>

        <div class="form-group">
            <label for="name">Name</label>
            <?php echo form_input(array('name' => 'name', 'class' => 'form-control'), set_value('name')); ?>
        </div>

        <div class="form-group">
            <label for="email">Email address</label>
            <?php echo form_input(array('name' => 'email', 'class' => 'form-control', 'type' => 'email')); ?>
        </div>


        <div class="form-group">
            <label for="password">Password</label>
            <?php echo form_input(array('name' => 'password', 'class' => 'form-control', 'type' => 'password')); ?>
        </div>

        <div class="form-group">
            <label for="password_confirm">Password Confirm</label>
            <?php echo form_input(array('name' => 'password_confirm', 'class' => 'form-control', 'type' => 'password')); ?>
        </div>

        <div class="form-group">
            <label for="date_birthday">Date Birthday</label>
            <?php echo form_input(array('name' => 'date_birthday', 'class' => 'datepicker form-control', 'id' => 'date_birthday')); ?>
        </div>
        <div class="checkbox">
            <label>
                <?php echo form_checkbox(array('name' => 'active', 'value' => 1)); ?>
            </label>
        </div>
        <button type="submit" class="btn btn-success">Create</button>
    </form>
</div>

