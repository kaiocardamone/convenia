<?php echo validation_errors('<div class="alert alert-danger" role="alert">','</div>');

?>

<h1>Adicionar Categoria</h1>
<div class="col-lg-4">
    <form action="http://convenia.dev/CategoryController/create" method="post" accept-charset="utf-8">

            <div class="form-group">
                <label for="name">Name</label>
                <?php echo form_input(array('name' => 'name', 'class' => 'form-control'), set_value('name'),'autofocus'); ?>
            </div>

            <div class="checkbox">
                <label>
                    <?php echo form_checkbox(array('name' => 'active', 'value' => 1, 'checked' => true)); ?>
                </label>
            </div>
            <button type="submit" class="btn btn-success">Active</button>
    </form>
</div>

