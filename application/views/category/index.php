<?php
if($this->session->flashdata('categoryInsertOk')){
    echo '<div class="alert alert-success" role="alert">'.$this->session->flashdata('categoryInsertOk').'</div>';
}
if($this->session->flashdata('categoryUpdateOk')){
    echo '<div class="alert alert-success" role="alert">'.$this->session->flashdata('categoryUpdateOk').'</div>';
}
if($this->session->flashdata('categoryUpdateError')){
    echo '<div class="alert alert-danger" role="alert">'.$this->session->flashdata('categoryUpdateError').'</div>';
}
if($this->session->flashdata('categoryDeleteOk')){
    echo '<div class="alert alert-success" role="alert">'.$this->session->flashdata('categoryDeleteOk').'</div>';
}
if($this->session->flashdata('categoryDeleteError')){
    echo '<div class="alert alert-danger" role="alert">'.$this->session->flashdata('categoryDeleteError').'</div>';
}
?>
    <h1>List of Categories</h1>

<div class="panel panel-default">
    <!-- Default panel contents -->
    <div class="panel-heading">Categories</div>
    <table class="table">
        <thead>
        <tr>
            <th>#</th>
            <th>Name</th>
            <th>Created</th>
            <th>Updated</th>
            <th>Active</th>
            <th>Actions</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach($categories as $category) { ?>
            <tr>
                <th scope="row"><?php echo $category->id; ?></th>
                <td><?php echo $category->name; ?></td>
                <td><?php echo $category->created > 0 ? dateChangeHelper::enDateChangeToBrDate($category->created) : '-'; ?></td>
                <td><?php echo $category->updated > 0 ? dateChangeHelper::enDateChangeToBrDate($category->updated) : '-'; ?></td>
                <td><?php echo $category->active == true ? 'Yes' : 'No'; ?></td>
                <td>
                    <a href="/CategoryController/update/<?php echo $category->id; ?>"><span class="glyphicon glyphicon-edit"></span></a>
                    <a href="/CategoryController/delete/<?php echo $category->id; ?>"><span class="glyphicon glyphicon-remove"></span></a>
                    <!--                        <a href="/admin/employees/view/1"></a>-->
                </td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
</div>
<?php echo $pagination; ?>
<p><? echo "Number of records returned this page: " . sizeof( $category ); ?></p>
