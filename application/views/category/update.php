<h1>Edit Category</h1>
<div class="col-lg-4">
    <form action="http://convenia.dev/CategoryController/update/<? echo $category->id; ?>" method="post" accept-charset="utf-8">
        <?php echo validation_errors('<div class="alert alert-danger" role="alert">','</div>');?>
        <div class="form-group">
            <label for="name">Name</label>
            <?php echo form_input(array('name' => 'name', 'class' => 'form-control', 'value' => $category->name), 'autofocus'); ?>
        </div>

        <div class="form-group">
            <?php echo form_checkbox(array('name' => 'active', 'value' => 1, 'checked' => $category->active != 0 ? true : false)); ?>
            <label>Active</label>
        </div>
        <button type="submit" class="btn btn-success">Update</button>
    </form>
</div>

