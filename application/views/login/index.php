<?php
if($this->session->flashdata('loginError')){
    echo '<div class="alert alert-danger" role="alert">'.$this->session->flashdata('loginError').'</div>';
}

echo validation_errors('<div class="alert alert-danger" role="alert">','</div>');

?>
<div class="content-form-header">
    <h2>CONVENiA</h2>
    <h5>Insert your credentians</h5>
</div>
<div class="content-form-wrapper">
    <span class="top-form">
    <i class="glyphicon glyphicon-user"></i>
    </span>
    <form class="content-form" action="/LoginController/login" method="POST">
        <input class="content-form-control" autocomplete="on" placeholder="E-mail" type="text" required autofocus name="email">
        <input class="content-form-control" autocomplete="on" placeholder="Password" type="password" required name="password">
        <input class="content-form-control" autocomplete="on" placeholder="Enviar" type="submit">
    </form>
