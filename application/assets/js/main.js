

$(document).ready(function () {

    $('.datepicker').datepicker({
        autoclose: true,
        format: 'dd/mm/yyyy'
    });

});

function changePassword(id)
{
    if($('#password').val() != $('#confirmPassword').val()) {
        alert('Password not match');
        return false;
    }
    $.ajax({
        'url' : "http://convenia.dev/UserController/changePassword",
        'data' :'id='+id+'&password='+$('#password').val()+'&confirmPassword='+$('#confirmPassword').val(),
        'type' : 'POST'
    }).done(function( msg ) {
        var response = jQuery.parseJSON(msg);
        console.log(response);
        if(response.changed) {
            $('#response').addClass('bg-success');
        } else {
            $('#response').addClass('bg-danger');
        }
        $('.hiddenPassword').addClass('hide');
        $('#response').html(response.message);
    });
}

function showPasswordUpdate()
{
    $('.hiddenPassword').removeClass('hide');
}


