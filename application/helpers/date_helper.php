<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class dateChangeHelper{

    public static function brDateChangeToEnDate($date)
    {
        $date = date(implode('-', array_reverse(explode('/', $date))));
        $date = new DateTime($date);
        return $date->format('Y-m-d');
    }

    public static function enDateChangeToBrDate($date)
    {
        $date = new DateTime($date);
        return $date->format('d/m/Y');
    }

}
