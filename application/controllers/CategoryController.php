<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class CategoryController extends CI_Controller {

    const TYPE_VALIDATION_EDIT = 'edit';
    const TYPE_VALIDATION_CREATE = 'create';
    const MAX_PAGINATION = 3;

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
    public function __construct(){
        parent::__construct();
        $this->load->helper("form");
        $this->load->helper("array");
        $this->load->library("form_validation");
        $this->load->helper('date_helper');
        $this->load->library('session');
        $this->load->library('table');
        $this->load->library('template');
        $this->load->library('pagination');
        $this->load->model('CategoryModel');
    }

	public function index()
	{

        $max = self::MAX_PAGINATION;
        $ini = (!$this->uri->segment(self::MAX_PAGINATION)) ? 0 : $this->uri->segment(self::MAX_PAGINATION);

        $config['base_url'] = '/CategoryController/index';
        $config['total_rows'] = $this->CategoryModel->countCategories();
        $config['per_page'] = $max;

        $this->pagination->initialize($config);

        $data["pagination"] = $this->pagination->create_links();
        $data["categories"] = $this->CategoryModel->returnCategories($max, $ini);

        return $this->template->show('category/index', $data);

	}

    public function create()
    {
        $this->validation(self::TYPE_VALIDATION_CREATE);

        if($this->form_validation->run() == TRUE) {
            $data = elements(array('name', 'active'), $this->input->post());
            $this->load->model('CategoryModel');
            if($this->CategoryModel->insert($data)) {
                $this->session->set_flashdata('categoryInsertOk','The category has been registered');
                redirect('CategoryController/index');
            }
        }
        $this->load->library('template');

        echo $this->template->show('category/create');
    }

    public function update($id = null)
    {

        if(empty($id)) {
            redirect('CategoryController/index');
        }

        $category = Array('category' => $this->CategoryModel->findById($id)->row());

        $this->validation(self::TYPE_VALIDATION_EDIT);

        if($this->form_validation->run() == TRUE){
            $data = elements(array('name', 'active'), $this->input->post());
            if($this->CategoryModel->update($data, $id)){
                $this->session->set_flashdata('categoryUpdateOk','The category has been updated');
                redirect('CategoryController/index');
            }
            $this->session->set_flashdata('categoryUpdateError','The category not been updated');
            redirect('CategoryController/index');
        }

        return $this->template->show('category/update', $category);
    }

    public function delete($id)
    {
        $this->load->library('template');
        $this->load->model('CategoryModel');

        if($this->CategoryModel->delete($id)) {
            $this->session->set_flashdata('categoryDeleteOk','The category has been deleted');
            redirect('CategoryController/index');
        }
        $this->session->set_flashdata('categoryDeleteError','To delete the category, you need to update all users using this');
        redirect('CategoryController/index');
    }

    public function validation($type)
    {
        $this->form_validation->set_rules('active','ACTIVE');
        if($type == self::TYPE_VALIDATION_CREATE) {
            $this->form_validation->set_rules('name','NAME','trim|required|max_length[45]|min_length[3]|ucwords|is_unique[category.name]');
        }
    }
}