<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class UserController extends CI_Controller {

    const TYPE_VALIDATION_EDIT = 'edit';
    const TYPE_VALIDATION_CREATE = 'create';
    const MAX_PAGINATION = 3;

    private $user;

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
    public function __construct(){
        parent::__construct();
        $this->load->helper("form");
        $this->load->helper("array");
        $this->load->helper('date');
        $this->load->helper('date_helper');
        $this->load->library("form_validation");
        $this->load->library('session');
        $this->load->library('table');
        $this->load->library('template');
        $this->load->library('pagination');
        $this->load->model('UserModel');
    }

	public function index()
	{

        $max = self::MAX_PAGINATION;
        $ini = (!$this->uri->segment(self::MAX_PAGINATION)) ? 0 : $this->uri->segment(self::MAX_PAGINATION);

        $config['base_url'] = '/UserController/index';
        $config['total_rows'] = $this->UserModel->countUsers();
        $config['per_page'] = $max;

        $this->pagination->initialize($config);

        $data["pagination"] = $this->pagination->create_links();
        $data["users"] = $this->UserModel->returnUsers($max, $ini);

        return $this->template->show('user/index', $data);
	}

    public function create()
    {
        $this->validation(self::TYPE_VALIDATION_CREATE);

        if($this->form_validation->run() == TRUE) {
            $data = elements(array('category_id', 'name', 'email', 'password', 'date_birthday', 'active'), $this->input->post());
            $data['date_birthday'] = dateChangeHelper::brDateChangeToEnDate($this->input->post('date_birthday'));
            if($this->UserModel->insert($data)) {
                $this->session->set_flashdata('insertok','The user has been registered');
                redirect('UserController/index');
            }
        }

        $this->load->model('CategoryModel');
        $category = Array('category' => $this->CategoryModel->listCategory()->result());

        return $this->template->show('user/create', $category);
    }

    public function update($id = null)
    {
        if(empty($id)) {
            return redirect('user/index');
        }

        $user = Array('user' => $this->UserModel->findById($id)->row());

        $this->validation(self::TYPE_VALIDATION_EDIT);

        if($this->form_validation->run() == TRUE){
            $data = elements(array('category_id', 'name', 'date_birthday', 'active'), $this->input->post());
            $data['date_birthday'] = dateChangeHelper::brDateChangeToEnDate($this->input->post('date_birthday'));
            if($this->UserModel->update($data, $id)){
                $this->session->set_flashdata('updateok','The user has been updated');
                redirect('UserController/index');
            }
        }

        $user['user']->date_birthday = dateChangeHelper::enDateChangeTobrDate(date($user['user']->date_birthday));
        $this->load->model('CategoryModel');
        $user['user']->categories = $this->CategoryModel->listCategory()->result();
        return $this->template->show('user/update', $user);
    }

    public function delete($id)
    {

        if($this->UserModel->delete($id)) {
            $this->session->set_flashdata('deleteOk','The user has been deleted');
            redirect('UserController/index');
        }

        $this->session->set_flashdata('deleteError','The user not been deleted');
        redirect('UserController/index');
    }

    public function changePassword()
    {

        if($this->input->post('password') != $this->input->post('confirmPassword')) {
            echo json_encode(array('message' => 'Passwords not match', 'changed' => false));
            exit();
        }

        if($this->UserModel->changePassword($this->input->post('id'), $this->input->post('password'))){
            echo json_encode(array('message' => 'Password has ben changed', 'changed' => true));
            exit();
        }

        echo json_encode(array('message' => 'Error when try to change password', 'changed' => false));
        exit();
    }

    public function validation($type)
    {
        $this->form_validation->set_rules('category_id','CATEGORY','required');
        $this->form_validation->set_rules('name','NAME','trim|required|max_length[45]|min_length[3]|ucwords');
        if($type == self::TYPE_VALIDATION_CREATE) {
            $this->form_validation->set_rules('email','EMAIL','trim|required|max_length[200]|min_length[8]|valid_email|strtolower|is_unique[user.email]');
            $this->form_validation->set_rules('password','PASSWORD','trim|required|');
            $this->form_validation->set_rules('password_confirm','PASSWORD CONFIM','trim|required|matches[password]');
        }

        if($type == self::TYPE_VALIDATION_EDIT) {
            if(!empty($password) && !empty($passwordConfirm)) {
                $this->form_validation->set_rules('password_confirm','PASSWORD CONFIM','trim||matches[password]');
                $this->form_validation->set_rules('email','EMAIL','trim|required|max_length[200]|min_length[8]|valid_email|strtolower|is_unique[user.email]');
            }
        }
    }
}