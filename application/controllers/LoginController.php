<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class LoginController extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
    public function __construct(){
        parent::__construct();
        $this->load->helper("form");
        $this->load->helper("array");
        $this->load->library("form_validation");
        $this->load->library('template');
    }

	public function index()
	{
        $this->load->library('template');
        return $this->template->show('login/index');
	}

    public function login()
    {
        $this->validation();

        if($this->form_validation->run() == TRUE) {
            $this->load->model('UserModel');
            $loggedUser = $this->UserModel->userLogin($this->input->post('email'), $this->input->post('password'));
            if($loggedUser) {
                $this->session->set_userdata(
                    array('user' =>
                        array(
                            'user_id'   => $loggedUser->id,
                            'user_name' => $loggedUser->name,
                            'user_email' => $loggedUser->email
                        )
                    )
                );
                $this->session->set_flashdata('loginSuccess','Welcome');
               redirect('UserController');
            }
        }

        $this->session->set_flashdata('loginError','Bad Credentials');

        $this->load->library('template');

        return $this->template->show('login/index');
    }

    public function validation()
    {
        $this->form_validation->set_rules('email','EMAIL','trim|required|max_length[200]|min_length[8]|valid_email|strtolower');
        $this->form_validation->set_rules('password','PASSWORD','trim|required|');

    }

    public function unsetSession(){
        $this->session->sess_destroy();
        redirect(base_url());
    }
}