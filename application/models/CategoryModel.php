<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class CategoryModel extends CI_Model{

    public function insert($data = NULL){

        if($data != NULL){

            $data['created'] = date('Y-m-d H:i:s');
            $data['updated'] = date('Y-m-d H:i:s');
            if($this->db->insert('category', $data)){
                return true;
            }

        }

        return FALSE;

    }

    public function update($data = NULL, $id){

        if($data != NULL && $id != NULL){

            $data['updated'] = date('Y-m-d H:i:s');

            if($this->db->update('category', $data, array('id' => $id))){
                return true;
            }

        }

        return FALSE;

    }

    public function delete($id){

        if($id != NULL){
            if($this->findUsageById($id)) {
               return false;
            }

            if($this->db->delete('category', array('id' => $id))){
                return true;
            }

        }

        return FALSE;

    }

    public function findById($id = NULL){

        if($id != NULL){

            $this->db->where('id', $id);
            $this->db->limit(1);
            return $this->db->get('category');

        }

        return FALSE;

    }

    public function findUsageById($id = NULL){

        if($id != NULL){

            $this->db->select('us.id as user_id, us.name as user_name, us.email, us.date_birthday, us.created, us.updated, us.active, ca.name as category_name, ca.id as category_id');
            $this->db->from('category AS ca');
            $this->db->join('user AS us','ca.id = us.category_id', 'inner');
            $this->db->where('ca.id', $id);
            $this->db->limit(1);
            return $this->db->get('user')->result();

        }

        return FALSE;

    }

    public function listCategory(){
        return $this->db->get('category');
    }

    function returnCategories($max, $ini)
    {
        $query = $this->db->get('category', $max, $ini);
        return $query->result();
    }

    function countCategories()
    {
        return $this->db->count_all_results('category');
    }

}

?>