<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class UserModel extends CI_Model{

    public function insert($data = NULL){

        if(empty($data) && empty($id)) {
            return FALSE;
        }

        $data['created'] = date('Y-m-d H:i:s');
        $data['updated'] = date('Y-m-d H:i:s');
        $data['password'] = md5($data['password']);
        if($this->db->insert('user', $data)){
            return true;
        }

    }

    public function update($data = NULL, $id){

        if(empty($data) && empty($id)) {
            return FALSE;
        }

        $data['updated'] = date('Y-m-d H:i:s');

        if($this->db->update('user', $data, array('id' => $id))){
            return true;
        }

        return FALSE;

    }

    public function delete($id)
    {

        if(empty($id)){
            return FALSE;
        }

        if($this->db->delete('user', array('id' => $id))){
            return true;
        }

        return FALSE;

    }

    public function findById($id = NULL)
    {

        if(empty($id)) {
            return FALSE;
        }

        $this->db->where('id', $id);
        $this->db->limit(1);
        return $this->db->get('user');

    }

    public function userLogin($email, $password)
    {

        if(empty($email) || empty($password)) {
            return FALSE;
        }

        $this->db->where('email', $email);
        $this->db->where('password', md5($password));
        return $this->db->get('user')->row(0);

    }

    public function listUser()
    {
        $this->db->select('*');
        $this->db->from('user');
        $this->db->join('category','category.id = user.category_id');
        return $this->db->get();
    }

    function returnUsers($max, $ini)
    {
        $this->db->select('us.id as user_id, us.name as user_name, us.email, us.date_birthday, us.created, us.updated, us.active, ca.name as category_name, ca.id as category_id');
        $this->db->from('user AS us');
        $this->db->join('category AS ca','us.category_id = ca.id');
        return $this->db->get('user', $max, $ini)->result();
    }

    function countUsers()
    {
        return $this->db->count_all_results('user');
    }

    public function changePassword($id, $password){
        if(empty($password) || empty($id)) {
            return FALSE;
        }
        $this->db->where('id', $id);
        if($this->db->update('user', array('password' => md5($password)))){
            return true;
        }

        return FALSE;

    }

}

?>