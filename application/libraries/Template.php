<?php

class Template extends CI_Controller{

    const LOGIN_VIEW = 'login/index';

    function __construct(){
        $this->CI =& get_instance();
    }

    function show($view, $data = null){

        $this->CI->load->view('includes/header');
        if($view != self::LOGIN_VIEW) {
            $this->CI->load->view('includes/menu');
            $this->validateUserSession();
        }

        $this->CI->load->view($view, $data);
        $this->CI->load->view('includes/footer');
    }

    public function ValidateUserSession() {
        $user = $this->CI->session->userdata('user');
        if(empty($user['user_name']) || empty($user['user_email'])) {
            $this->CI->session->sess_destroy();
            redirect('LoginController/index');
        }
    }

}